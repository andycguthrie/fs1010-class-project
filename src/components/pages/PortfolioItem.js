import React, { useState, useEffect } from "react";
import { Container, Button } from 'reactstrap';
const PortfolioItem = (props) => {
  const [portfolio, setPortfolio] = useState([]);

  let id = props.match.params.id;
  
  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/api/portfolio/${id}`);
      res
        .json()
        .then((res) => setPortfolio(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [id]);
  return (
    <Container className='main'>
      <h1 className="xlarge-pages">{portfolio.project}</h1>
      <h2 className="xlarge-pages">{portfolio.client}</h2>
     <Button color="secondary">{portfolio.tag1}</Button>{' '},
     <Button color="secondary">{portfolio.tag2}</Button>{' '}
     <Button color="secondary">{portfolio.tag3}</Button>{' '}
      <h1 className="xlarge-pages">{portfolio.name}</h1>
        <div>
            <img src={portfolio.photo} alt={portfolio.name} width="100%" height="auto" />
        </div>   
        <div>
        <ul>
          <li>{portfolio.b1}</li>
        <li>{portfolio.b2}</li>
        <li>{portfolio.b3}</li>
        <li>{portfolio.b4}</li>
        <li>{portfolio.b5}</li>
        </ul>
        </div> 
    </Container>
  );
};
export default PortfolioItem;