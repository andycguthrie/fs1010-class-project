import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { Container, Row, Col} from 'reactstrap'

const Portfolio = () => {
    const [portfolio, setPortfolio]=useState([])
    const history = useHistory();
    
    useEffect(() => {
        async function fetchData() {
          const res = await fetch("http://localhost:4000/api/portfolio");
          res
            .json()
            .then((res) => setPortfolio(res))
            .catch((err) => console.log(err));
        }
        fetchData();
      }, []);

      const PortfolioRoute = (e, portfolioObject) => {
        e.preventDefault();
        history.push(`/portfolio/${portfolioObject.id}`)
      }

    return (
        <Container className='main'>
           <h1>Portfolio</h1>
           {portfolio.map((portfolioObject)=>(
               <div key={portfolioObject}>
         <Row className="my-5">
            <Col lg="7">
            <img src={portfolioObject.photo} alt="" width="100%" height="auto"/>
            </Col>
            <Col lg="5">
            <h1 onClick={(e) => PortfolioRoute(e, portfolioObject)} className="xlarge-pages"> {portfolioObject.client}</h1>

    </Col>
    </Row>



        

               </div>
           ))}
    </Container>
    )
}

export default Portfolio