import React from 'react'
import { Container, Row, Col} from 'reactstrap'
import andyPhoto from '../images/andy_photo.jpg'

const Resume = () => {
    return (
        <Container className='main'>
            <h1> Resume</h1>
        <Row className="my-5">
        <Col>
        <h2 className="xlarge-pages"> WORK EXPERIENCE </h2>
            <h2>PRINT / WEB DESIGN / MARKETING</h2>
            <ul class="resume-points">
              <li>Improved branding standards with branding style guides and leading workshops to develop best practices</li>
                <li> Researched, presented and implemented 
            marketing plans</li>
            <li> Prepared, administered and analyzed 
            client needs assessments to identify 
            communication, marketing and design 
            strategies</li>
            <li> Developed and presented creative briefs for 
            creative teams and agencies to ensure 
            accurate creative direction</li>
            <li> Designed websites using HTML, CSS and 
            responsive design</li>
            <li> Produced and designed logos, posters, 
            brochures, flyers, newsletters, annual reports, 
            magazines, books and displays</li>
            <li>  Prepared and executed social media plans 
            and strategies</li>
            <li> Comfortable and experienced at presenting 
            design concepts and direction to all levels of 
            senior management</li>
            <li> Worked close with print suppliers insuring 
            projects are on time and budget</li>
            <li>  Managed freelance web developers, 
            photographers, copy writers and 
            videographers for consistent project 
            execution</li>
            <li> Managed calendars for large projects 
            including scheduling and notifications</li>
            <li> Maintained a photo library for easy access and 
            search of client images</li>
            <li> Prepared original written copy for advertising, 
            newsletter and website projects</li>
            <li> Analyzed and prepared strategies based on 
            Google Analytics statistics</li></ul>
            <h2>ANIMATION / VIDEOGRAPHY / PHOTOGRAPHY </h2>
            <ul class="resume-points">

<li> Adapted lighting, camera and exposure settings
as the on-set cinematographer at documentary
and commercial video shoots</li>
<li>Art directed and photographed images for
campaign and design projects including image
manipulation and colour correction</li>
<li>Managed and produced animation projects
alongside writers, animators, art directors,
talent and clients</li>
<li>Finalized and edited video projects for
television and online media use</li>
<li>Arranged time lines, models, location and
rental equipment for project related
photography and video</li>
<li>Prepared and presented storyboards for
concept approval</li>
</ul>

<h2>SOFTWARE </h2>
<ul class="resume-points">
<li>Expert with Adobe Indesign, Adobe Illustrator,
Adobe Photoshop, Adobe Premiere, Adobe
After Effects and Sketch</li>
<li>Experienced with Word, Excel, Powerpoint,
Adobe Flash (Animate), Dreamweaver, Final Cut
Pro, FTP software, Adobe Character Animate,
Adobe XD</li></ul>

<h2 className="xlarge-pages"> CAREER HIGHLIGHTS</h2>
<h2> UNIVERSITY OF TORONTO - STUDENT LIFE REBRANDING</h2>
    <ul class="resume-points">
   <li> Analyzed needs of 13 departments within U of T Student Life and prepared needs analysis presentation for Communications Director</li>
   <li>Defined key problem with the brand and developed a proof of concept presentation</li>
    <li>Presented proof of concept to 13 different lead teams gaining buy in from all</li>
    <li>Improved productivity and brand consistency across Social, Multimedia, Web and Print marketing material</li>
    <li>Liaised with design agency for design and collateral deliverables</li>
</ul>
  <h2> THE CAMPFIRE COLLECTIVE- BLOG REDESIGN</h2> 
    <ul class="resume-points">
        <li>Prepared animated and functional prototype for blog that outlined design, content organization and functionality using Sketch and Invision</li>
     <li>Interpreted strategic brand needs into a simplified filtering user interface</li>
        <li>Designed page layouts that met the User Experience needs</li></ul>
   <h2> PAOC - TESTIMONY MAGAZINE</h2>
    <ul class="resume-points">
        <li>Art directed and designed the monthly publication for the past 10 years.</li>
            <li>Initiated and lead process for publication re- design with senior management, editorial and creative team three times</li>
    <li>Prepared and finalized a proposal to reduce design and production costs by $30,000, while maintaining quality of 
    Publication</li>
</ul>
    <h2> SOCHANGE - MOSQUITOES SUCK TOUR</h2>
   <h4> Award: RGD Social Good Design Award, 2012</h4>
    <ul class="resume-points">
 <li>Contracted animation expert and gave feedback on execution</li>
 <li>Collaborated with director, writer and art director to create storyboard and final script</li>
 <li>Prepared marketing and production plan for digital and traditional media buy</li>
 <li>Managed art direction for branding and campaign collateral</li>
</ul>     
<h2>PETERBOROUGH LAKEFIELD COMMUNITY POLICE SERVICE - PROJECT T.A.C.T.I.C</h2> 
  <h4>  Award: Ontario Chief of Police Award, 2013</h4>
  <ul class="resume-points">
   <li> Taught 32 students in a college learning environment the concept and creation process for public service announcements</li>
   <li>Facilitated students’ creation of 13 videos</li>
    <li>Collaborated with client to prepare a social media and marketing strategy that integrated the students’ work</li>
        <li>Improved awareness around “Digital Citizenship” in Ontario, as videos are being viewed by thousands of elementary and high school students</li>

</ul>
    
  <h2>  PETERBOROUGH REGIONAL HEALTH CENTRE CLOSER CAMPAIGN</h2>
   <h4> Award: Peterborough Business Excellence Award -
    Marketing and Promotion, 2011</h4>
    <ul class="resume-points">
   <li> Interpreted client’s needs into a creative brief</li>
   <li>Arranged and briefed creative team on design and photography needs</li> 
   <li>Consolidated creative team input into presentation for client </li>
    <li> Directed photography for campaign images</li>
        <li>  Collaborated with outside agency for campaign logo branding and style guide integration</li>
            <li> Achieved more than campaign goal in the amount of $3.4 million</li>
</ul>
   <h2> U19 WORLD LACROSSE CHAMPIONSHIP</h2>
   <h4> Award: Honourable Mention, Peterborough Civic Award for Sport Betterment, 2008</h4>
    <ul class="resume-points">
   <li> Consulted large executive committee for creative deliverable needs</li>
   <li>Directed and managed content with representatives from 13 different countries</li>
   <li>Designed and coordinated manufacturing for all publications, promotional collateral and merchandise</li>
</ul>      
            
            </Col>
            
            <Col>
            <blockquote class="zitat1">
  Thanks for all the great stuff you brought to the table. We are better off
            now because of you.
<cite>ANTHONY BOWMAN</cite>
</blockquote>
                <img className="img-fluid rounded mb-4 mb-lg-0" src={andyPhoto} alt="" />
            </Col>
           
         
        </Row>
    </Container>
    )
}

export default Resume