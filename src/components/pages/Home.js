import React from 'react'
import background from '../images/andy_photo_wide.jpg'
import { Container, Row, Col, Button } from 'reactstrap'
import { Link } from 'react-router-dom'

const Home = () => {
    return(
    <Container >
      <Row>
                <Col lg="12">
                 <div className="homeCopy">
                     <h1 className="xlarge glow">Hello!</h1>
                    < h1 className="largeWhite"> Have we met?</h1>
                    <div className="oneThirdWidth">
                        
                        <Link to="/about">
                        <Button  className="orange" size="lg" block>More About Me</Button>{''}</Link></div>
                     </div>  
                     <img className="homeBackground" src={background} alt="" />  
                    
                </Col>
               
            </Row>
          
    </Container>
    )
}

export default Home



