import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import andyPhoto from '../images/andy_photo.jpg'

const About = () => {
    return (
        <Container className='main'>
            <h1> About Me</h1>
        <Row className="my-5">
            <Col>
                <img className="img-fluid rounded mb-4 mb-lg-0" src={andyPhoto} alt="" />
            </Col>
            <Col>
            <h1 className="xlarge-pages"> I know a lot!</h1>
  <h2 className="xlarge-pages">About getting things done.</h2>
  <p>To put things simply, I’m the Swiss Army Knife of design and web production and management. With over 15 years of experience, I’ve worked on projects from responsive websites to large publications and everything in-between. I’m a creative graphic design and marketing professional who knows a lot about getting things done. As a Graphic Designer, Art Director, Web Designer, Photographer, Animator, Video Editor and Marketer I have learned something about myself. I love solving problems.

  Contact me about your next project.
  </p>
  <blockquote class="zitat1">
 Grizzly is her name. Andy is the one on the right.
<cite>Andy C. Guthrie</cite>
</blockquote>
            </Col>
        </Row>
    </Container>
    )
}

export default About